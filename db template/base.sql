create table if not exists public.ingredients
(
    id   serial not null
        constraint ingredients_pk
            primary key,
    name varchar
);

comment on table public.ingredients is 'Таблица с ингридентами';

comment on column public.ingredients.name is 'Название ингридиента';

alter table public.ingredients
    owner to postgres;

create unique index if not exists ingredients_id_uindex
    on public.ingredients (id);

create table if not exists public.users
(
    id         serial      not null
        constraint users_pk
            primary key,
    username   varchar(30) not null,
    actualname varchar,
    email      varchar,
    phonenum   varchar
);

comment on column public.users.username is 'Имя пользователя';

comment on column public.users.actualname is 'Настоящие имя пользователя';

comment on column public.users.email is 'Почта пользователя';

comment on column public.users.phonenum is 'Номер телефона';

alter table public.users
    owner to admin;

create unique index if not exists users_id_uindex
    on public.users (id);

create unique index if not exists users_username_uindex
    on public.users (username);

create table if not exists public.userextend
(
    id            serial    not null
        constraint userextend_pk
            primary key
        constraint userextend_users_id_fk
            references public.users
            on delete cascade,
    age           integer,
    high          double precision,
    weight        double precision,
    iswoman       boolean,
    diettype      integer,
    ratio         double precision,
    dailyneed     double precision,
    excludeding   integer[] not null,
    proteins      double precision,
    fats          double precision,
    carbohydrates double precision
);

comment on column public.userextend.age is 'Возраст';

comment on column public.userextend.high is 'Рост';

comment on column public.userextend.weight is 'Вес';

comment on column public.userextend.iswoman is 'Принадлежность к полу';

comment on column public.userextend.diettype is 'Тип диеты';

comment on column public.userextend.ratio is 'Коэффициент';

comment on column public.userextend.dailyneed is 'Суточная потребность';

comment on column public.userextend.excludeding is 'Исключенные ингредиенты';

comment on column public.userextend.proteins is 'Суточная потребность в белках';

comment on column public.userextend.fats is 'Суточная потребность в жирах';

comment on column public.userextend.carbohydrates is 'Суточная потребность в углеводах';

alter table public.userextend
    owner to admin;

create unique index if not exists userextend_id_uindex
    on public.userextend (id);

create table if not exists public.sprtyperecipes
(
    id       serial not null
        constraint sprtyperecipes_pk
            primary key,
    typename varchar
);

comment on table public.sprtyperecipes is 'Справочник типов рецептов';

comment on column public.sprtyperecipes.typename is 'Имя типа рецепта';

alter table public.sprtyperecipes
    owner to admin;

create table if not exists public.recipes
(
    id                 serial    not null
        constraint recipes_pk
            primary key,
    title              varchar   not null,
    ingredients        integer[] not null,
    detail             varchar,
    recipestype        integer
        constraint recipes_sprtyperecipes_id_fk
            references public.sprtyperecipes,
    calories           double precision,
    proteins           double precision,
    fats               double precision,
    carbohydrates      double precision,
    weight_ingredients double precision[]
);

comment on table public.recipes is 'Рецепты';

comment on column public.recipes.title is 'Название рецепта';

comment on column public.recipes.ingredients is 'Коды ингридиентов';

comment on column public.recipes.detail is 'Текст рецепта';

comment on column public.recipes.recipestype is 'Тип рецепта';

comment on column public.recipes.calories is 'Кол-во калорий на 100 грамм';

comment on column public.recipes.proteins is 'Белки на 100гр.';

comment on column public.recipes.fats is 'Жиры на 100гр.';

comment on column public.recipes.carbohydrates is 'Углеводы на 100гр.';

comment on column public.recipes.weight_ingredients is 'Вес ингридиентов';

alter table public.recipes
    owner to postgres;

create unique index if not exists recipes_id_uindex
    on public.recipes (id);

create unique index if not exists sprtyperecipes_id_uindex
    on public.sprtyperecipes (id);

create table if not exists public.posts
(
    id     serial not null
        constraint posts_pk
            primary key,
    title  varchar,
    detail varchar
);

comment on table public.posts is 'Посты от пользователей';

comment on column public.posts.title is 'Заголовок';

comment on column public.posts.detail is 'Тело поста';

alter table public.posts
    owner to admin;

create unique index if not exists posts_id_uindex
    on public.posts (id);

create table if not exists public.comments
(
    id     serial                not null
        constraint comments_pk
            primary key,
    idpost integer               not null,
    iduser integer               not null,
    detail varchar,
    rating integer default 0,
    hidden boolean default false not null
);

comment on table public.comments is 'Коментарии к постам';

comment on column public.comments.idpost is 'Внешний ключ на пост';

comment on column public.comments.iduser is 'Внешний ключ на пользователя
';

comment on column public.comments.detail is 'Тело коментария';

comment on column public.comments.rating is 'Рейтинг коментария';

comment on column public.comments.hidden is 'Коментарий скрыт';

alter table public.comments
    owner to admin;

create unique index if not exists comments_id_uindex
    on public.comments (id);

