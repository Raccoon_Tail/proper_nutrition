package com.service.develop.properNutrition;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import com.service.develop.properNutrition.service.calculations.calcCalories.CalcCaloriesService;
import com.service.develop.properNutrition.service.ingredients.IngredientsService;
import com.service.develop.properNutrition.service.recipes.RecipesService;
import com.service.develop.properNutrition.service.user.UserService;
import com.service.develop.properNutrition.service.userExtend.UserExtendService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProperNutritionApplicationTests {

	@Autowired
	public RecipesService recipesService;

	@Autowired
	public IngredientsService ingredientsService;

	@Autowired
	public CalcCaloriesService calcCaloriesService;

	@Autowired
	public UserService userService;

	@Autowired
	public UserExtendService userExtendService;


	@Test
	void dbTest() {
		System.out.println(recipesService.findAll());
		System.out.println(ingredientsService.findAll());
		System.out.println(userService.findAll());
		System.out.println(userExtendService.findAll());
		System.out.println(recipesService.getAllPossibleRecipesUser(1));
	}

	@Test
	void calcCaloriesTest(){
		UserExtend userExtend = new UserExtend();
		userExtend.setAge(42);
		userExtend.setHigh(150f);
		userExtend.setWeight(50f);
		userExtend.setWoman(true);
		userExtend.setDietType(2);
		userExtend.setRatio(1.2f);

		System.out.println(calcCaloriesService.makeCalcCalories(userExtend));
	}

	@Test
	void testRation(){
		UserExtend userExtend = new UserExtend();
		userExtend.setId(1);
		System.out.println(recipesService.getPreparedRecipesUser(userExtend, 3));
		System.out.println(recipesService.getPreparedRecipesUser(userExtend, 4));
		System.out.println(recipesService.getPreparedRecipesUser(userExtend, 6));
	}

//	@Test
//	void populateDatabase(){
//
//	}

}
