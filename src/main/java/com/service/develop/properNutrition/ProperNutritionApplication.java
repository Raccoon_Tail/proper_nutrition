package com.service.develop.properNutrition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

@SpringBootApplication
public class ProperNutritionApplication extends WebMvcAutoConfiguration {

	public static void main(String[] args) {
		SpringApplication.run(ProperNutritionApplication.class, args);
		//AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DbConfig.class);
	}

}
