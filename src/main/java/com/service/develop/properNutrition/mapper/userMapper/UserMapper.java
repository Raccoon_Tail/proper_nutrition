package com.service.develop.properNutrition.mapper.userMapper;

import com.service.develop.properNutrition.entity.user.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong("id"));
        user.setUserName(resultSet.getString("username"));
        user.setActualName(resultSet.getString("actualname"));
        user.setEmail(resultSet.getString("email"));
        user.setPhoneNum(resultSet.getString("phonenum"));
        return user;
    }
}
