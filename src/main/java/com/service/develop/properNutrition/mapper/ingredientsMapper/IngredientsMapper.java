package com.service.develop.properNutrition.mapper.ingredientsMapper;

import com.service.develop.properNutrition.entity.ingredients.Ingredients;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class IngredientsMapper implements RowMapper<Ingredients> {

    @Override
    public Ingredients mapRow(ResultSet resultSet, int i) throws SQLException {
        Ingredients ingredients = new Ingredients();
        ingredients.setId(resultSet.getInt("id"));
        ingredients.setName(resultSet.getString("name"));
        return ingredients;
    }
}
