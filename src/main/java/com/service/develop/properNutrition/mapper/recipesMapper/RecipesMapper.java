package com.service.develop.properNutrition.mapper.recipesMapper;

import com.service.develop.properNutrition.entity.recipes.Recipes;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RecipesMapper implements RowMapper<Recipes> {

    @Override
    public Recipes mapRow(ResultSet resultSet, int i) throws SQLException {
        Recipes recipes = new Recipes();
        recipes.setId(resultSet.getInt("id"));
        recipes.setTitle(resultSet.getString("title"));
        recipes.setIngredients((Integer[]) resultSet.getArray("ingredients").getArray());
        recipes.setDetail(resultSet.getString("detail"));
        recipes.setRecipesType(resultSet.getInt("recipestype"));
        recipes.setRecipesTypeName(resultSet.getString("typename"));
        recipes.setCalories(resultSet.getFloat("calories"));
        recipes.setProteins(resultSet.getFloat("proteins"));
        recipes.setFats(resultSet.getFloat("fats"));
        recipes.setCarbohydrates(resultSet.getFloat("carbohydrates"));
        recipes.setWeightIngredients((Double[]) resultSet.getArray("weight_ingredients").getArray());
        return recipes;
    }
}
