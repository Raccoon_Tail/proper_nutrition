package com.service.develop.properNutrition.mapper.userExtendMapper;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserExtendMapper implements RowMapper<UserExtend> {
    @Override
    public UserExtend mapRow(ResultSet resultSet, int i) throws SQLException {
        UserExtend userExtend = new UserExtend();
        userExtend.setId(resultSet.getLong("id"));
        userExtend.setAge(resultSet.getInt("age"));
        userExtend.setHigh(resultSet.getFloat("high"));
        userExtend.setWeight(resultSet.getFloat("weight"));
        userExtend.setWoman(resultSet.getBoolean("iswoman"));
        userExtend.setDietType(resultSet.getInt("diettype"));
        userExtend.setRatio(resultSet.getFloat("ratio"));
        userExtend.setDailyNeed(resultSet.getFloat("dailyneed"));
        userExtend.setExcludeIng((Integer[]) resultSet.getArray("excludeding").getArray());
        return userExtend;
    }
}
