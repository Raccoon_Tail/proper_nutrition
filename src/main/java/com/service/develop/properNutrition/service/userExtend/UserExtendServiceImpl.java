package com.service.develop.properNutrition.service.userExtend;

import com.service.develop.properNutrition.dao.userExtend.UserExtendDao;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserExtendServiceImpl implements UserExtendService {

    @Autowired
    public UserExtendDao userExtendDao;

    @Override
    public List<UserExtend> findAll() {
        return userExtendDao.findAll();
    }

    @Override
    public void update(UserExtend userExtend) {
        userExtendDao.update(userExtend);
    }

    @Override
    public UserExtend getById(int id) {
        return userExtendDao.getById(id);
    }
}
