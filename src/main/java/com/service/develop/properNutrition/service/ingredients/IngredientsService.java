package com.service.develop.properNutrition.service.ingredients;

import com.service.develop.properNutrition.entity.ingredients.Ingredients;

import java.util.List;

public interface IngredientsService {
    List<Ingredients> findAll();

    void update(Ingredients ingredients);

    void delete(int id);

    void insert(Ingredients ingredients);

    Ingredients getById(int id);
}
