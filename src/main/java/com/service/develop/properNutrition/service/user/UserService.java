package com.service.develop.properNutrition.service.user;

import com.service.develop.properNutrition.entity.user.User;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;

import java.util.List;

public interface UserService {
    List<User> findAll();

    void update(User user);

    void delete(int id);

    void insert(User user, UserExtend userExtend);

    User getById(int id);
}
