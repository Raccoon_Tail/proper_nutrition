package com.service.develop.properNutrition.service.calculations.calcCalories;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import org.springframework.stereotype.Service;

@Service
public class CalcCaloriesServiceBasicImpl implements CalcCaloriesService {

    @Override
    public UserExtend makeCalcCalories(UserExtend userExtend) {
        float voo = 9.99f * userExtend.getWeight() + 6.25f * userExtend.getHigh() - 4.92f * userExtend.getAge();
        if (userExtend.isWoman()){
            voo -= 161;
        } else {
            voo +=5;
        }
        //Расчет СПК
        userExtend.setDailyNeed(
                //Расчет ВOO
                (voo) * userExtend.getRatio()
        );
        switch (userExtend.getDietType()) {
            case 1: //Поддержание веса
                userExtend.setProteins(1.45f * userExtend.getWeight());
                userExtend.setFats(1.1f * userExtend.getWeight());
                userExtend.setCarbohydrates(
                        (userExtend.getDailyNeed() - 4.1f * userExtend.getProteins() - 9.1f * userExtend.getFats()) / 4.1f);
                break;
            case 2: //Набор массы
                userExtend.setProteins(2f * userExtend.getWeight());
                userExtend.setFats(0.8f * userExtend.getWeight());
                userExtend.setCarbohydrates(
                        ((userExtend.getDailyNeed() * 1.2f)
                                - 4.1f * userExtend.getProteins() - 9.1f * userExtend.getFats()) / 4.1f);
                break;
            case 3: //Снижение веса
                userExtend.setProteins(1.45f * userExtend.getWeight());
                userExtend.setFats(1.1f * userExtend.getWeight());
                userExtend.setCarbohydrates(
                        ((userExtend.getDailyNeed() * 0.8f)
                                - 4.1f * userExtend.getProteins() - 9.1f * userExtend.getFats()) / 4.1f);
                break;
        }
        return userExtend;
    }
}
