package com.service.develop.properNutrition.service.recipes;

import com.service.develop.properNutrition.entity.recipes.Recipes;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;

import java.util.List;

public interface RecipesService {

    List<Recipes> findAll();

    void update(Recipes recipes);

    void delete(int id);

    void insert(Recipes recipes);

    Recipes getById(int id);

    List<Recipes> getAllPossibleRecipesUser(long userId);

    List<Recipes> getPreparedRecipesUser(UserExtend userExtend, int quantity);

}
