package com.service.develop.properNutrition.service.user;

import com.service.develop.properNutrition.dao.user.UserDao;
import com.service.develop.properNutrition.entity.user.User;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserDao userDao;

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public void delete(int id) {
        userDao.delete(id);
    }

    @Override
    @Transactional
    public void insert(User user, UserExtend userExtend) {
        userDao.insert(user, userExtend);
    }

    @Override
    public User getById(int id) {
        return null;
    }
}
