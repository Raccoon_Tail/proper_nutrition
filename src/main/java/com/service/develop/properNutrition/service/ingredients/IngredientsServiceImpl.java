package com.service.develop.properNutrition.service.ingredients;

import com.service.develop.properNutrition.dao.ingredients.IngredientsDao;
import com.service.develop.properNutrition.entity.ingredients.Ingredients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientsServiceImpl implements IngredientsService {

    @Autowired
    public IngredientsDao ingredientsDao;

    @Override
    public List<Ingredients> findAll() {
        return ingredientsDao.findAll();
    }

    @Override
    public void update(Ingredients ingredients) {
        ingredientsDao.update(ingredients);
    }

    @Override
    public void delete(int id) {
        ingredientsDao.delete(id);
    }

    @Override
    public void insert(Ingredients ingredients) {
        ingredientsDao.insert(ingredients);
    }

    @Override
    public Ingredients getById(int id) {
        return ingredientsDao.getById(id);
    }
}
