package com.service.develop.properNutrition.service.userExtend;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;

import java.util.List;

public interface UserExtendService {
    List<UserExtend> findAll();

    void update(UserExtend userExtend);

    UserExtend getById(int id);
}
