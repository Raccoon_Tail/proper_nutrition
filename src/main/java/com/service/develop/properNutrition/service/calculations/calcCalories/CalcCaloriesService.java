package com.service.develop.properNutrition.service.calculations.calcCalories;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;

public interface CalcCaloriesService {

    UserExtend makeCalcCalories(UserExtend userExtend);
}
