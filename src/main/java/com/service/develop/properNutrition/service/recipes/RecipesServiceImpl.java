package com.service.develop.properNutrition.service.recipes;

import com.service.develop.properNutrition.dao.ingredients.IngredientsDao;
import com.service.develop.properNutrition.dao.recipes.RecipesDao;
import com.service.develop.properNutrition.entity.ingredients.Ingredients;
import com.service.develop.properNutrition.entity.recipes.Recipes;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import com.service.develop.properNutrition.service.ingredients.IngredientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecipesServiceImpl implements RecipesService {

    public final RecipesDao recipesDao;

    @Autowired
    public RecipesServiceImpl(RecipesDao recipesDao) {
        this.recipesDao = recipesDao;
    }

    @Autowired
    public IngredientsService ingredientsService;

    @Override
    public List<Recipes> findAll() {
        return recipesDao.findAll();
    }

    @Override
    public void update(Recipes recipes) {
        recipesDao.update(recipes);
    }

    @Override
    public void delete(int id) {
        recipesDao.delete(id);
    }

    @Override
    public void insert(Recipes recipes) {
        recipesDao.insert(recipes);
    }

    @Override
    public Recipes getById(int id) {
        return recipesDao.getById(id);
    }

    @Override
    public List<Recipes> getAllPossibleRecipesUser(long userId) {
        return recipesDao.getAllPossibleRecipes(userId);
    }

    @Override
    public List<Recipes> getPreparedRecipesUser(UserExtend userExtend, int quantity) {
        List<Recipes> preparedRecipes = new ArrayList<>();
        List<Integer> listOfRationType = new ArrayList<>();
        switch (quantity){
            case 4:
                listOfRationType.add(1);
                listOfRationType.add(3);
                listOfRationType.add(4);
                listOfRationType.add(5);
                break;
            case 5:
                listOfRationType.add(1);
                listOfRationType.add(2);
                listOfRationType.add(3);
                listOfRationType.add(4);
                listOfRationType.add(5);
                break;
            case 6:
                listOfRationType.add(1);
                listOfRationType.add(2);
                listOfRationType.add(3);
                listOfRationType.add(4);
                listOfRationType.add(5);
                listOfRationType.add(6);
                break;
            default:
                listOfRationType.add(1);
                listOfRationType.add(3);
                listOfRationType.add(5);
                break;
        }
        for (int rationType: listOfRationType){
            Recipes recipes = recipesDao.getPossibleRecipeByType(userExtend.getId(), rationType);
            List<String> ingredientsNameList = new ArrayList<>();
            for (int ingredientType : recipes.getIngredients()){
                ingredientsNameList.add(ingredientsService.getById(ingredientType).getName());
            }
            recipes.setIngredientsName(new String[ingredientsNameList.size()]);
            ingredientsNameList.toArray(recipes.getIngredientsName());
            preparedRecipes.add(recipes);
        }

        return preparedRecipes;
    }


}
