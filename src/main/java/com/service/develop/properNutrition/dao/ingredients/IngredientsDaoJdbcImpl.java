package com.service.develop.properNutrition.dao.ingredients;

import com.service.develop.properNutrition.entity.ingredients.Ingredients;
import com.service.develop.properNutrition.mapper.ingredientsMapper.IngredientsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class IngredientsDaoJdbcImpl implements IngredientsDao {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public IngredientsDaoJdbcImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Ingredients> findAll() {
        String sql = "SELECT * FROM ingredients";
        return jdbcTemplate.query(sql, new IngredientsMapper());
    }

    @Override
    public void update(Ingredients ingredients) {
        String sql = "UPDATE ingredients SET name = ? WHERE id = ?";
        jdbcTemplate.update(sql, ingredients.getName(), ingredients.getId());
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM ingredients WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void insert(Ingredients ingredients) {
        String sql = "INSERT INTO ingredients (name) VALUES (?)";
        jdbcTemplate.update(sql, ingredients.getName());
    }

    @Override
    public Ingredients getById(int id) {
        String sql = "SELECT * FROM ingredients WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new IngredientsMapper(), id);
    }
}
