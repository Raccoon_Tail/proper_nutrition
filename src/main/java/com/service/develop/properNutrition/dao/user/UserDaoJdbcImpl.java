package com.service.develop.properNutrition.dao.user;

import com.service.develop.properNutrition.entity.user.User;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import com.service.develop.properNutrition.mapper.userMapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDaoJdbcImpl implements UserDao {

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDaoJdbcImpl(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        String sql = "SELECT * FROM users";
        return jdbcTemplate.query(sql, new UserMapper());
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE users SET username = ?, actualname = ?, email = ?, phonenum = ?";
        jdbcTemplate.update(sql, user.getUserName(), user.getActualName(), user.getEmail(), user.getPhoneNum());
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM users WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void insert(User user, UserExtend userExtend) {
        String insertUser = "INSERT INTO users (username, actualname, email, phonenum) VALUES (?, ?, ?, ?)";
        String insertUserExtend = "INSERT INTO userextend (" +
                "id, " +
                "age, " +
                "high, " +
                "weight, " +
                "iswoman, " +
                "diettype, " +
                "ratio, " +
                "dailyneed, " +
                "excludeding) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Integer idUser = jdbcTemplate.queryForObject(
                insertUser,
                Integer.class,
                user.getUserName(),
                user.getActualName(),
                user.getEmail(),
                user.getPhoneNum());
        jdbcTemplate.update(
                insertUserExtend,
                idUser,
                userExtend.getAge(),
                userExtend.getHigh(),
                userExtend.getWeight(),
                userExtend.getDietType(),
                userExtend.getRatio(),
                userExtend.getDailyNeed(),
                userExtend.getExcludeIng());
    }

    @Override
    public User getById(int id) {
        String sql = "SELECT * FROM users WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new UserMapper(), id);
    }
}
