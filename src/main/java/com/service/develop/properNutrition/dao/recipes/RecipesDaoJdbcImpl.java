package com.service.develop.properNutrition.dao.recipes;

import com.service.develop.properNutrition.entity.recipes.Recipes;
import com.service.develop.properNutrition.mapper.recipesMapper.RecipesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RecipesDaoJdbcImpl implements RecipesDao{


    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public RecipesDaoJdbcImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Recipes> findAll() {
        String sql = "SELECT * FROM recipes ORDER BY id";
        return jdbcTemplate.query(sql, new RecipesMapper());
    }

    @Override
    public void update(Recipes recipes) {
        String sql = "UPDATE recipes SET title = ?, ingredients = ?, detail = ? WHERE id = ?";
        jdbcTemplate.update(sql, recipes.getTitle(), recipes.getIngredients(), recipes.getDetail(), recipes.getId());
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM recipes WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    @Override
    public void insert(Recipes recipes) {
        String sql = "INSERT INTO recipes (title, ingredients, detail) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, recipes.getTitle(), recipes.getIngredients(), recipes.getDetail());
    }

    @Override
    public Recipes getById(int id) {
        String sql = "SELECT * FROM recipes WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new RecipesMapper(), id);
    }

    @Override
    public List<Recipes> getAllPossibleRecipes(long userId) {
        String sql = "SELECT " +
                "recipes.id, " +
                "recipes.title, " +
                "recipes.ingredients, " +
                "recipes.detail, " +
                "recipes.recipestype, " +
                "public.sprtyperecipes.typename, " +
                "recipes.calories, " +
                "recipes.proteins, " +
                "recipes.fats, " +
                "recipes.carbohydrates, " +
                "recipes.weight_ingredients " +
                "FROM recipes " +
                "INNER JOIN public.sprtyperecipes on recipes.recipestype = public.sprtyperecipes.id " +
                "WHERE " +
                "NOT ingredients && (SELECT public.userextend.excludeding FROM userextend WHERE userextend.id = ?) " +
                "AND recipestype = ? " +
                "ORDER BY random() " +
                "LIMIT 1";
        return jdbcTemplate.query(sql, new RecipesMapper(), userId);
    }

    @Override
    public Recipes getPossibleRecipeByType(long userId, int recipeType) {
        String sql = "SELECT " +
                "recipes.id, " +
                "recipes.title, " +
                "recipes.ingredients, " +
                "recipes.detail, " +
                "recipes.recipestype, " +
                "public.sprtyperecipes.typename, " +
                "recipes.calories, " +
                "recipes.proteins, " +
                "recipes.fats, " +
                "recipes.carbohydrates, " +
                "recipes.weight_ingredients " +
                "FROM recipes " +
                "INNER JOIN public.sprtyperecipes on recipes.recipestype = public.sprtyperecipes.id " +
                "WHERE " +
                "NOT ingredients && (SELECT public.userextend.excludeding FROM userextend WHERE userextend.id = ?) " +
                "AND recipestype = ? " +
                "ORDER BY random() " +
                "LIMIT 1";
        return jdbcTemplate.queryForObject(sql, new RecipesMapper(), userId, recipeType);
    }
}
