package com.service.develop.properNutrition.dao.user;

import com.service.develop.properNutrition.entity.user.User;
import com.service.develop.properNutrition.entity.userExtend.UserExtend;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    void update(User user);

    void delete(int id);

    void insert(User user, UserExtend userExtend);

    User getById(int id);

}
