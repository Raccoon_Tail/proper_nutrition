package com.service.develop.properNutrition.dao.userExtend;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;

import java.util.List;

public interface UserExtendDao {

    List<UserExtend> findAll();

    void update(UserExtend userExtend);

    UserExtend getById(int id);
}
