package com.service.develop.properNutrition.dao.userExtend;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import com.service.develop.properNutrition.mapper.recipesMapper.RecipesMapper;
import com.service.develop.properNutrition.mapper.userExtendMapper.UserExtendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserExtendDaoJdbcImpl implements UserExtendDao {

    public JdbcTemplate jdbcTemplate;

    @Autowired
    public UserExtendDaoJdbcImpl(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<UserExtend> findAll() {
        String sql = "SELECT * FROM userextend";
        return jdbcTemplate.query(sql, new UserExtendMapper());
    }

    @Override
    public void update(UserExtend userExtend) {
        String sql = "UPDATE userextend SET " +
                                            "age = ?, " +
                                            "high = ?, " +
                                            "weight = ?, " +
                                            "iswoman = ?, " +
                                            "diettype = ?, " +
                                            "ratio = ?, " +
                                            "dailyneed = ?, " +
                                            "excludeding = ?";
        jdbcTemplate.update(sql,
                userExtend.getAge(),
                userExtend.getHigh(),
                userExtend.getWeight(),
                userExtend.isWoman(),
                userExtend.getDietType(),
                userExtend.getRatio(),
                userExtend.getDailyNeed(),
                userExtend.getExcludeIng());
    }

    @Override
    public UserExtend getById(int id) {
        String sql = "SELECT * FROM userextend WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new UserExtendMapper(), id);
    }
}
