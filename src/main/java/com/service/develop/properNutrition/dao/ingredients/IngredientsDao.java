package com.service.develop.properNutrition.dao.ingredients;

import com.service.develop.properNutrition.entity.ingredients.Ingredients;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IngredientsDao {

    List<Ingredients> findAll();

    void update(Ingredients ingredients);

    void delete(int id);

    void insert(Ingredients ingredients);

    Ingredients getById(int id);
}
