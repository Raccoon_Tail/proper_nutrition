package com.service.develop.properNutrition.dao.recipes;

import com.service.develop.properNutrition.entity.recipes.Recipes;

import java.util.List;

public interface RecipesDao{

    List<Recipes> findAll();

    void update(Recipes recipes);

    void delete(int id);

    void insert(Recipes recipes);

    Recipes getById(int id);

    List<Recipes> getAllPossibleRecipes(long userId);

    Recipes getPossibleRecipeByType(long userId , int recipeType);
}
