package com.service.develop.properNutrition.entity.recipes;


import java.util.Arrays;

public class Recipes {

    private long id;
    private String title;
    private Integer[] ingredients;
    private String detail;
    private String[] ingredientsName;
    private int recipesType;
    private String recipesTypeName;
    private float calories;
    private float proteins;
    private float fats;
    private float carbohydrates;
    private Double[] weightIngredients;


    public Recipes() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(Integer[] ingredients) {
        this.ingredients = ingredients;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String[] getIngredientsName() {
        return ingredientsName;
    }

    public void setIngredientsName(String[] ingredientsName) {
        this.ingredientsName = ingredientsName;
    }

    public int getRecipesType() {
        return recipesType;
    }

    public void setRecipesType(int recipesType) {
        this.recipesType = recipesType;
    }

    public float getCalories() {
        return calories;
    }

    public void setCalories(float calories) {
        this.calories = calories;
    }

    public float getProteins() {
        return proteins;
    }

    public void setProteins(float proteins) {
        this.proteins = proteins;
    }

    public float getFats() {
        return fats;
    }

    public void setFats(float fats) {
        this.fats = fats;
    }

    public float getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(float carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Double[] getWeightIngredients() {
        return weightIngredients;
    }

    public void setWeightIngredients(Double[] weightIngredients) {
        this.weightIngredients = weightIngredients;
    }

    public String getRecipesTypeName() {
        return recipesTypeName;
    }

    public void setRecipesTypeName(String recipesTypeName) {
        this.recipesTypeName = recipesTypeName;
    }

    @Override
    public String toString() {
        return "Recipes{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", ingredients=" + Arrays.toString(ingredients) +
//                ", detail='" + detail + '\'' +
                ", ingredientsName=" + Arrays.toString(ingredientsName) +
                ", recipesType=" + recipesType +
                ", recipesTypeName='" + recipesTypeName + '\'' +
                ", calories=" + calories +
                ", proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                ", weightIngredients=" + Arrays.toString(weightIngredients) +
                '}';
    }
}
