package com.service.develop.properNutrition.entity.userExtend;

import java.util.Arrays;

public class UserExtend {
    private long id;
    private int age;
    private float high;
    private float weight;
    private boolean isWoman;
    private int dietType;
    private float ratio;
    private float dailyNeed;
    private Integer[] excludeIng;
    private float proteins;
    private float fats;
    private float carbohydrates;

    public UserExtend() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getHigh() {
        return high;
    }

    public void setHigh(float high) {
        this.high = high;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isWoman() {
        return isWoman;
    }

    public void setWoman(boolean woman) {
        isWoman = woman;
    }

    public int getDietType() {
        return dietType;
    }

    public void setDietType(int dietType) {
        this.dietType = dietType;
    }

    public float getRatio() {
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public float getDailyNeed() {
        return dailyNeed;
    }

    public void setDailyNeed(float dailyNeed) {
        this.dailyNeed = dailyNeed;
    }

    public Integer[] getExcludeIng() {
        return excludeIng;
    }

    public void setExcludeIng(Integer[] excludeIng) {
        this.excludeIng = excludeIng;
    }

    public float getProteins() {
        return proteins;
    }

    public void setProteins(float proteins) {
        this.proteins = proteins;
    }

    public float getFats() {
        return fats;
    }

    public void setFats(float fats) {
        this.fats = fats;
    }

    public float getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(float carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    @Override
    public String toString() {
        return "UserExtend{" +
                "id=" + id +
                ", age=" + age +
                ", high=" + high +
                ", weight=" + weight +
                ", isWoman=" + isWoman +
                ", dietType=" + dietType +
                ", ratio=" + ratio +
                ", dailyNeed=" + dailyNeed +
                ", excludeIng=" + Arrays.toString(excludeIng) +
                ", proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                '}';
    }
}
