package com.service.develop.properNutrition.controller;

import com.service.develop.properNutrition.entity.userExtend.UserExtend;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping("/index")
    public String homePage(Model model) {
        UserExtend userExtend = new UserExtend();
        userExtend.setDietType(1);
        userExtend.setWoman(true);
        userExtend.setRatio(1.2f);
        model.addAttribute("userextend", userExtend);
        return "index";
    }

    @PostMapping("/index")
    public String calcCalories(@ModelAttribute("userextend") UserExtend userExtend){
        System.out.println(userExtend);
        return "/index";
    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }
    @GetMapping("/404")
    public String error404() {
        return "/error/404";
    }

}
